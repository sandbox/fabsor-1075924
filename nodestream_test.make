; $Id$

; API

api = 2

; Core

core = 6.x

; NodeStream

projects[nodestream][type] = profile
projects[nodestream][download][type] = git
projects[nodestream][download][url] = http://git.drupal.org/project/nodestream.git
projects[nodestream][download][revision] = 6.x-1.x

; Contrib

projects[devel][version] = 1.23
projects[diff][version] = 2.1
projects[simpletest][version] = 2.11
